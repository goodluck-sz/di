import * as path from 'path';
import { ContainerBuilder, YamlFileLoader, JsFileLoader } from 'node-dependency-injection';

const args = process.argv;
const typeFile = args[2];

if (typeFile === 'json') {

	const pathJson = path.join(__dirname, 'config', 'file.json');
	const container = new ContainerBuilder();
	const loaderJson = new JsFileLoader(container);
	loaderJson.load(pathJson);
	const mailerJson = container.get('service.mailer');
	console.log("Input Index file Json: ", mailerJson);

} else if (typeFile === 'yml') {

	const pathYml = path.join(__dirname, 'config', 'file.yml');
	const container = new ContainerBuilder();
	const loaderYml = new YamlFileLoader(container);
	loaderYml.load(pathYml);
	const mailerYml = container.get('service.mailer');
	console.log("Input Index file Yml: ", mailerYml);

}
