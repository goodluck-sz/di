HOW RUN:
- build project:
```
npm install
```

- Start project:
```
npm run start:json
```
or
```
npm run start:yml
```

- Input console:
```
di@1.0.0 start C:\Users\serg\work\edetek\di
tsc && node ./src/index.js

Input ExampleService: hello world
Input Mailer:  service.example
Input Index file:  Mailer {}
```
